class Todo {
  constructor(id = null, title, description, status) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.status = status;
  }
}

module.exports = Todo;
const cron = require('node-cron');

module.exports = function initScheduler(todoService) {
    cron.schedule('*/5 * * * * *', async () => {
        const todoList = await todoService.listTodo();
        todoList.forEach(element => console.log(element.title));
    });
}
const { Sequelize, Model, DataTypes } = require("sequelize");
const sequelize = new Sequelize("sqlite::memory:");

exports.Todo = sequelize.define("todos", {
  id: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
  title: DataTypes.STRING,
  description: DataTypes.STRING,
  status: DataTypes.STRING
});

(async () => {
  await sequelize.sync({ force: true });
  // Code here
})();
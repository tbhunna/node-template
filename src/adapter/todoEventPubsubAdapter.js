const { PubSub } = require('@google-cloud/pubsub');

const pubSub = new PubSub();
const topicName = 'done-todo';

class EventEmitterAdapter {
  constructor() {
    this.pubSub = new PubSub();
    this.topicName = 'todo';
  }

  async emitToDoCreatedEvent(todo) {
    try {
      const dataBuffer = Buffer.from(JSON.stringify(todo));
      await this.pubSub.topic(this.topicName).publish(dataBuffer);
    } catch (e) {
      console.error('Error while emit event to pub/sub');
      throw e;
    }
  }
}

module.exports = EventEmitterAdapter;
class TodoTransport {
    constructor(id = id, title, description, status) {
      this.id = id;
      this.title = title;
      this.description = description;
      this.status = status;
    }
  }
  
  module.exports = TodoTransport;
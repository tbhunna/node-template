# NODE-TEMPLATE

A template node backend service demonstrate package layout of node backend service.


## Service's external dependencies

 - Google cloud pubsub: this service expected two topics need to be created, `todo` and `done-todo`
 - sqlite3 in-momory database

## How to run

set google's creadential file location

    export GOOGLE_APPLICATION_CREDENTIALS={your localtion of credentail file}

install package

    npm install

run service

    npm start

## Application layer
![application layer](https://gitlab.com/simye1/node-template/-/raw/master/diagram/hexagonal%20architecture.png)
- **port and adapter** This layer will handle all comunication between service and external party both input and output, input part migth be HTTP API call, listen to message queue, scheduler, command line interface. The o
- **service** This layler handle all business logic.
- **model** Application model layer.

## Package layout

 - **api** contains api entry point related code and componant like, express's roter, controller all HTTP related like , request, response end here.
 - **event_habdeler** contains event or message queue handeler entry point, this package responible just for extract data from message that send from message queue and make a call to servce layer for farture processing.
 - **scheduler** contains job scheduler related code entry point, all scheduler library/framwork are live only in here.
 - **cli** contains code that support calling service operation by comamd line interface entry point, code is this package just responsible for parse the argument from command line interface and make a cll to service layer for farture processing.
 - **service** contails all business logic related code.
 - **adapter** contains code that handle comunication to external component or service like relational database, message queue, third party API.
 - **model** contails all data model that use for process our business logic.

